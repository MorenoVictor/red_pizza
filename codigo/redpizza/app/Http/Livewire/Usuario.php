<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Usuario extends Component
{
    public function render()
    {
        return view('livewire.usuario');
    }
}
